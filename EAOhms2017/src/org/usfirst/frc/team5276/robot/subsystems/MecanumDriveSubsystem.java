package org.usfirst.frc.team5276.robot.subsystems;

import org.usfirst.frc.team5276.robot.Robot;
import org.usfirst.frc.team5276.robot.RobotMap;
import org.usfirst.frc.team5276.robot.commands.MecanumTeleopCommand;
import org.usfirst.frc.team5276.robot.utilities.XBOXController;

import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.interfaces.Gyro;

/**
 *
 */
public class MecanumDriveSubsystem extends Subsystem {

	RobotDrive myDrive = new RobotDrive(RobotMap.frontLeft, RobotMap.frontRight, RobotMap.backLeft, RobotMap.backRight);
//	public Gyro gyro = new AnalogGyro(RobotMap.gyro);
	// gyro removed temporarily to find right code
    // Put methods for controlling this subsystem
    // here. Call these from Commands.

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
    	setDefaultCommand(new MecanumTeleopCommand());
        //setDefaultCommand(new MySpecialCommand());
    }
    
    public void mecanumDrive(XBOXController joystick){
    	myDrive.mecanumDrive_Cartesian(joystick.leftAnalogX(), joystick.leftAnalogY(), joystick.rightAnalogX(), 0); //gyro replaced with 0 for now
    }
    
    public void stop(){
    	myDrive.mecanumDrive_Cartesian(0, 0, 0, 0);
    }
}

