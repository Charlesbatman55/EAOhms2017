package org.usfirst.frc.team5276.robot.utilities;

import edu.wpi.first.wpilibj.Joystick;

public class XBOXController extends Joystick {
	
	
	
	public XBOXController(int port) {
		super(port);
		// TODO Auto-generated constructor stub
	}

//	Joystick XBOX;
//	/**
//	 * This allows us to use the XBOX controller directly. 
//	 * @param port This is the port of the XBOX Joystick. 
//	 */
//	public XBOXController() {
////		XBOX = new Joystick(port);
//	}
	
	public double leftAnalogX() {
		return getRawAxis(0);
	}
	
	public double leftAnalogY() {
		return -getRawAxis(1);
	}
	
	public double rightAnalogX() {
		return getRawAxis(4);
	}
	
	public double rightAnalogY() {
		return -getRawAxis(5);
	}
	
	public double getLB() {
		return getRawAxis(2);
	}
	
	public double getRB() {
		return getRawAxis(3);
	}
	
}
